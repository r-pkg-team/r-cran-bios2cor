Source: r-cran-bios2cor
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Shayan Doust <hello@shayandoust.me>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-bios2cor
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-bios2cor.git
Homepage: https://cran.r-project.org/package=Bios2cor
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-bio3d,
               r-cran-circular,
               r-cran-bigmemory,
               r-cran-igraph
Testsuite: autopkgtest-pkg-r

Package: r-cran-bios2cor
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R from Biological Sequences and Simulations to Correlation
 Analysis Utilities for computation and analysis of
 correlation/covariation in multiple sequence alignments and in
 side chain motions during molecular dynamics simulations. Features
 include the computation of correlation/covariation scores using a
 variety of scoring functions between either sequence positions in
 alignments or side chain dihedral angles in molecular dynamics
 simulations and utilities to analyze the correlation/covariation
 matrix through a variety of tools including network representation
 and principal components analysis. In addition, several utility
 functions are based on the R graphical environment to provide
 friendly tools for help in data interpretation. Examples of
 sequence covariation analysis are provided in: (1) Pele J, Moreau
 M, Abdi H, Rodien P, Castel H, Chabbert M (2014)
 <doi:10.1002/prot.24570> and (2) Taddese B, Deniaud M, Garnier A,
 Tiss A, Guissouma H, Abdi H, Henrion D, Chabbert M (2018) <doi:
 10.1371/journal.pcbi.1006209>. An example of side chain correlated
 motion analysis is provided in: Taddese B, Garnier A, Abdi H,
 Henrion D, Chabbert M (2020) <doi: 10.1038/s41598-020-72766-
 1>. This work was supported by the French National Research
 Agency (Grant number: ANR-11-BSV2-026) and by GENCI (Grant
 number: 100567).
